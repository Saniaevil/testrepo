var config = {
    paths: {
        'owlcarousel': "js/owl-carousel/owl.carousel"
    },
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};
