define(['jquery', 'owlcarousel'], function($) {
    return function(config, element) {
        $('.slider-images').owlCarousel({
            nav:false,
            dots:false,
            margin: 30,
            responsive:{
                0:{
                    items: 1
                },
                991:{
                    items: 2
                },
                1199:{
                    items: 3
                }
            }
        })
        // console.log(element)
        // console.log(config)

        console.log('carousel-block.js')
    };
});
// define(['jquery','<carousel_name>'], function($)
// {
//     return function(config, element)
//     {
//         $(element).<carousel_name>(config);
//     };
// });
