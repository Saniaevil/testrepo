define([
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'jquery',
    'ko',
    'underscore',
    'sidebar',
    'mage/translate',
    'mage/dropdown'
], function (Component, customerData, $, ko, _) {
    'use strict';

    return function (Component) {
        return Component.extend({

            /**
             * @override
             */
            initialize: function () {
                console.log('initialize');
                var self = this,
                    cartData = customerData.get('cart');

                if (cartData().hasOwnProperty('items')) {
                    cartData()['items'].forEach(function (item, i) {
                        cartData()['items'][i]['product_name'] = item['product_name'].replace(/&#039;/g, "'");
                    });
                }

                return this._super();
            },
        });
    }
});
